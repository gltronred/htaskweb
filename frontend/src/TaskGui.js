"use strict";

exports.showModalForm = function(vis) {
    return function() {
        if (vis) {
            $('#task-form').modal('show');
        } else {
            $('#task-form').modal('hide');
        }
    };
};

