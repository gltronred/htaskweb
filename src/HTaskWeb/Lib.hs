{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE TypeOperators #-}

module HTaskWeb.Lib where

import           Control.Monad
import           Control.Monad.IO.Class
import           Data.Aeson
import           Data.Aeson.Encoding (text)
import qualified Data.ByteString.Lazy.Char8 as B
import           Data.Char (toLower)
import qualified Data.Map as M
import           Data.Maybe
import           Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.Lazy as LT
import           Data.Text.Lazy.Encoding
import           Data.Time
import           GHC.Generics
import           Network.HTTP.Types
import           Network.Wai
import           Servant
import           Servant.API
import           System.Process
import           Text.Read (readMaybe)

newtype UUID = UUID { unUuid :: Text }
  deriving (Show, Read, FromHttpApiData, ToHttpApiData, FromJSON, ToJSON)

data TaskStatus
  = Pending
  | Deleted
  | Completed
  | Waiting
  | Recurring
  deriving (Generic, Read, Show, Enum)

instance FromJSON TaskStatus where
  parseJSON = genericParseJSON $ opts 0
instance ToJSON TaskStatus where
  toEncoding = genericToEncoding $ opts 0

instance ToHttpApiData TaskStatus where
  toUrlPiece Pending = "pending"
  toUrlPiece Deleted = "deleted"
  toUrlPiece Completed = "completed"
  toUrlPiece Waiting = "waiting"
  toUrlPiece Recurring = "recurring"
instance FromHttpApiData TaskStatus where
  parseUrlPiece t = case M.lookup (T.toLower t) m of
    Just ts -> Right ts
    Nothing -> Left $ "Incorrect TaskStatus: " <> T.take 10 t
    where m :: M.Map Text TaskStatus
          m = M.fromList [ (toUrlPiece ts, ts) | ts <- [Pending .. Recurring]]

data Priority = H | M | L deriving (Generic, Read, Show)

instance FromJSON Priority where
instance ToJSON Priority where
  toEncoding = genericToEncoding defaultOptions

newtype DateTime = DateTime {unDateTime :: UTCTime} deriving (Show, Read)
instance FromJSON DateTime where
  parseJSON = withText "DateTime" $ \s -> maybe (fail "DateTime should be in YYYYMMDDThhmmssZ") pure $ do
    let [y1,y2,y3,y4, m1,m2, d1,d2, 'T', h1,h2, mi1,mi2, s1,s2, 'Z'] = T.unpack s
    year <- readMaybe [y1,y2,y3,y4]
    mon <- readMaybe [m1,m2]
    day <- readMaybe [d1,d2]
    hour <- readMaybe [h1,h2]
    min <- readMaybe [mi1,mi2]
    sec <- readMaybe [s1,s2]
    pure $ DateTime $ UTCTime (fromGregorian year mon day) (fromIntegral $ hour*3600 + min*60 + sec)
instance ToJSON DateTime where
  toEncoding = text . dateTimeToInt
  toJSON = toJSON . dateTimeToInt

dateTimeToInt :: DateTime -> Text
dateTimeToInt (DateTime (UTCTime d t)) = let
  (year,mon,day) = toGregorian d
  (hh,mmss) = truncate t `divMod` 3600
  (mm,ss) = mmss `divMod` 60
  fi :: Int -> Integer
  fi = fromIntegral
  ymd = (year*100 + fi mon)*100 + fi day
  hms = (fi hh*100 + fi mm)*100 + fi ss
  lead = case 1 of
    _ | hms < 10 -> 5
      | hms < 100 -> 4
      | hms < 1000 -> 3
      | hms < 10000 -> 2
      | hms < 100000 -> 1
      | otherwise -> 0
  in T.concat [ T.pack $ show ymd, "T", T.replicate lead "0", T.pack $ show hms, "Z" ]

data Task = Task
  { status :: TaskStatus
  , uuid :: Maybe UUID          -- actually not Maybe
  , entry :: Maybe DateTime     -- actually not Maybe
  , description :: Text
  , start :: Maybe DateTime
  , end :: Maybe DateTime
  , due :: Maybe DateTime
  , until :: Maybe DateTime
  , wait :: Maybe DateTime
  , modified :: Maybe DateTime  -- actually not Maybe
  , scheduled :: Maybe DateTime
  , recur :: Maybe Text
  , mask :: Maybe Text
  , imask :: Maybe Int
  , parent :: Maybe UUID
  , project :: Maybe Text
  , priority :: Maybe Text
  , depends :: Maybe Text
  , tags :: Maybe [Text]
  , annotation :: Maybe [Value]
  , urgency :: Maybe Double     -- actually not Maybe
  } deriving (Generic, Read, Show)

instance FromJSON Task where
  parseJSON = genericParseJSON $ opts 0
instance ToJSON Task where
  toEncoding = genericToEncoding $ opts 0
  toJSON = genericToJSON $ opts 0

data TaskBody = TaskBody
  { taskDesc :: Text
  , taskTags :: [Text]
  , taskProject :: Maybe Text
  , taskDue :: Maybe UTCTime
  , taskWait :: Maybe UTCTime
  , taskStatus :: Maybe TaskStatus
  , taskRecur :: Maybe Text
  , taskParent :: Maybe UUID
  } deriving (Generic, Read, Show)

instance FromJSON TaskBody where
  parseJSON = genericParseJSON $ opts 4

type Api
  =    "api" :> "tasks" :> QueryParam "project" Text :> QueryParam "status" TaskStatus :> QueryParams "tag" Text :> QueryParams "notag" Text :> Get '[JSON] [Task]
  :<|> "api" :> "tasks" :> ReqBody '[JSON] TaskBody :> Post '[JSON] Task
  :<|> "api" :> "tasks" :> Capture "uuid" UUID :> Get '[JSON] Task
  :<|> "api" :> "tasks" :> Capture "uuid" UUID :> ReqBody '[JSON] TaskBody :> Post '[JSON] Task
  :<|> Raw

server :: FilePath -> FilePath -> Server Api
server task root = getTasks :<|> newTask :<|> getTask :<|> updTask :<|> static
  where
    static = serveDirectoryWebApp root

    getTasks :: Maybe Text -> Maybe TaskStatus -> [Text] -> [Text] -> Handler [Task]
    getTasks proj stat tags noTags = do
      r <- liftIO $ readTasks task proj tags noTags $ fromMaybe Pending stat
      fromEither r

    newTask :: TaskBody -> Handler Task
    newTask body = do
      euuid <- liftIO $ importTask task Nothing body
      uuid <- fromEither euuid
      getTask uuid

    getTask :: UUID -> Handler Task
    getTask uuid = do
      r <- liftIO $ readTask task uuid
      case r of
        Left e -> fromEither $ Left e
        Right Nothing -> throwError err404
        Right (Just x) -> pure x

    updTask :: UUID -> TaskBody -> Handler Task
    updTask uuid body = do
      _ <- liftIO $ importTask task (Just uuid) body
      getTask uuid

    fromEither :: Either String a -> Handler a
    fromEither (Left e) = throwError $ err500 { errBody = encodeUtf8 $ LT.pack e }
    fromEither (Right a) = pure a


readTasks :: FilePath           -- task executable
          -> Maybe Text         -- project
          -> [Text]             -- tags to include
          -> [Text]             -- tags to exclude
          -> TaskStatus         -- task status
          -> IO (Either String [Task])
readTasks task mproj plus minus stat = do
  let projFun = maybe id ((:) . ("project:" <>)) mproj
      tagsStr = map ("+"<>) plus ++ map ("-"<>) minus
      statStr = "status:" <> T.pack (show stat)
      limitStr = "limit:0"
      opts = (map T.unpack $ projFun $ statStr : limitStr : tagsStr) ++ ["export"]
  eitherDecode . encodeUtf8 . LT.pack <$> readProcess task opts ""

readTask :: FilePath -> UUID -> IO (Either String (Maybe Task))
readTask task (UUID uuid) = do
  let uuidStr = "uuid:" <> uuid
      limitStr = "limit:0"
      opts = [ T.unpack uuidStr, limitStr, "export" ]
  fmap listToMaybe . eitherDecode . encodeUtf8 . LT.pack <$> readProcess task opts ""

importTask :: FilePath -> Maybe UUID -> TaskBody -> IO (Either String UUID)
importTask taskProc muuid body = do
  let -- uuidFun = maybe id ((:) . T.unpack . unUuid) muuid
      bodyStr = LT.unpack $ decodeUtf8 $ encode $ bodyToTask muuid body
      opts = [ "import" ]
  res <- T.pack <$> readProcess taskProc opts bodyStr
  case T.words $ last $ T.lines res of
    ("add" : uuid : _rest) -> pure $ Right $ UUID uuid
    ("mod" : uuid : _rest) -> pure $ Right $ UUID uuid
    _ -> pure $ Left $ "Can't decode task answer: " <> T.unpack res

bodyToTask :: Maybe UUID -> TaskBody -> Task
bodyToTask muuid TaskBody{..} = Task
  { status = case taskStatus of
      Nothing -> maybe Pending (const Waiting) taskWait
      Just st -> st
  , uuid = muuid
  , description = taskDesc
  , project = taskProject
  , tags = if null taskTags then Nothing else Just taskTags
  , due = DateTime <$> taskDue
  , wait = DateTime <$> taskWait
  , entry = Nothing
  , start = Nothing
  , end = Nothing
  , until = Nothing
  , modified = Nothing
  , scheduled = Nothing
  , recur = taskRecur
  , mask = Nothing
  , imask = Nothing
  , parent = taskParent
  , priority = Nothing
  , depends = Nothing
  , annotation = Nothing
  , urgency = Nothing
  }

appMain :: FilePath -> FilePath -> Application
appMain task root = serve api $ server task root
  where api = Proxy :: Proxy Api

opts :: Int -> Options
opts k = defaultOptions
  { constructorTagModifier = map toLower
  , fieldLabelModifier = camelTo2 '-' . drop k
  , omitNothingFields = True
  }

