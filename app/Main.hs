{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeOperators #-}

module Main where

import Data.Function
import Data.Maybe
import GHC.Generics
import HTaskWeb.Lib
import Network.Wai.Handler.Warp
import Options.Generic

data Config w = Config
  { port :: w ::: Maybe Int    <?> "Port to listen on (default: 3000)"
  , root :: w ::: Maybe String <?> "Static root directory (default: ./static)"
  , task :: w ::: Maybe String <?> "Task executable (default: task)"
  } deriving (Generic)

instance ParseRecord (Config Wrapped)
deriving instance Show (Config Unwrapped)

main :: IO ()
main = do
  cfg <- unwrapRecord "htaskweb - opinionated web interface for taskwarrior"
  let dir = fromMaybe "./static" $ root cfg
      tsk = fromMaybe "task" $ task cfg
      settings = defaultSettings
        & setPort (port cfg & fromMaybe 3000)
        & setHost "127.0.0.1"
  runSettings settings $ appMain tsk dir

