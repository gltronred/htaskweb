module Main where

import Prelude
import Effect (Effect)
import Halogen.Aff as HA
import Halogen.VDom.Driver (runUI)

import TaskGui (ui, initState, tasksToState, updateTasks)

main :: Effect Unit
main = HA.runHalogenAff do
  body <- HA.awaitBody
  -- ts <- updateTasks
  -- let st = tasksToState ts initState
  runUI ui initState body
