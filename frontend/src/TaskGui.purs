module TaskGui where

import Prelude

import Data.Argonaut
import Data.Array
import Data.Array.NonEmpty as NArr
import Data.DateTime
import Data.DateTime.ISO
import Data.Either
import Data.Enum
import Data.Formatter.DateTime
import Data.Function
import Data.Int as Int
import Data.Maybe
import Data.Newtype
import Data.String as Str
import Data.String.CodeUnits as CU
import Data.Tuple
import Effect
import Effect.Aff (Aff)
import Effect.Class

import Affjax as AX
import Affjax.ResponseFormat as AXResponse
import Affjax.RequestBody as AXBody

import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP

foreign import showModalForm :: Boolean -> Effect Unit
showTaskForm :: Boolean -> Aff Unit
showTaskForm b = liftEffect $ showModalForm b

data Query a = Sync a
             | ChangeStatus String a
             | ChangeProject ProjName a
             | ToggleTag String a
             | ShowForm TaskBody a
             | UpdateDesc String a
             | UpdateProj String a
             | UpdateTags (Array String) a
             | UpdateDue  (Maybe DateTime) a
             | UpdateWait (Maybe DateTime) a
             | CloseForm a
             | SaveTask a
             | SetCompletion TaskBody Boolean a

data Task = Task
  { desc :: String
  , uuid :: String
  , status :: String
  , proj :: String
  , tags :: Array String
  , due :: Maybe DateTime
  , wait :: Maybe DateTime
  , urg :: Number
  , recur :: Maybe String
  , parent :: Maybe String
  }

instance decodeJsonTask :: DecodeJson Task where
  decodeJson json = do
    x <- decodeJson json
    desc <- x .: "description"
    uuid <- x .: "uuid"
    status <- x .: "status"
    proj <- x .:? "project" .!= "NA"
    tags <- x .:? "tags" .!= []
    due <- x .:? "due" <#> map unwrapISO
    wait <- x .:? "wait" <#> map unwrapISO
    urg <- x .: "urgency"
    recur <- x.:? "recur"
    parent <- x .:? "parent"
    pure $ Task { desc, uuid, status, proj, tags, due, wait, urg, recur, parent }

type TaskBody =
  { desc :: String
  , tags :: Array String
  , project :: String
  , due :: Maybe ISO
  , wait :: Maybe ISO
  , uuid :: Maybe String
  , status :: Maybe String
  , recur :: Maybe String
  , parent :: Maybe String
  }

emptyTaskBody :: TaskBody
emptyTaskBody = {desc: "", tags: [], project: "", due: Nothing, wait: Nothing, uuid: Nothing, status: Nothing, recur: Nothing, parent: Nothing}

taskToBody :: Task -> TaskBody
taskToBody (Task t) = { desc, tags, project, due, wait, uuid, status, recur, parent }
  where desc = t.desc
        tags = t.tags
        project = t.proj
        due = map ISO t.due
        wait = map ISO t.wait
        uuid = Just t.uuid
        status = Just t.status
        recur = t.recur
        parent = t.parent

type Tag = Tuple String Int

type ProjName = Tuple String (Maybe String)
type Proj = Tuple String Int
type Area = Tuple String (Array Proj)

type State =
  { tasks :: Array Task
  , projects :: Array Area
  , tags :: Array Tag
  , errors :: Array (Tuple String String)
  , taskFormBody :: Maybe TaskBody
  , selectedStatus :: String
  , selectedProject :: Maybe ProjName
  , selectedTags :: Array String
  }

initState :: State
initState = { tasks: [], projects: [], tags: []
            , errors: [ Tuple "warning" "Something went wrong. Synchronize to start working!"]
            , taskFormBody: Nothing
            , selectedStatus: "pending"
            , selectedProject: Nothing
            , selectedTags: []
            }

tasksToState :: Array Task -> State -> State
tasksToState ts s = s {tasks = ts, projects = constructAreas ts, tags = constructTags ts, errors = []}

updateTasks :: H.ComponentDSL State Query Void Aff Unit
updateTasks = do
  st <- H.get
  let url = ("/api/tasks?" <> "status=" <> st.selectedStatus)
  response <- H.liftAff $ AX.get AXResponse.json url
  case response.body of
    Left err -> H.modify_ (_ { errors = [ Tuple "danger" $ AXResponse.printResponseFormatError err ] })
    Right json -> case decodeJson json of
      Left err -> H.modify_ (_ { errors = [ Tuple "danger" err] })
      Right ts -> H.modify_ $ tasksToState ts

saveTask :: TaskBody -> Maybe Boolean -> H.ComponentDSL State Query Void Aff Unit
saveTask taskbody completed = do
  let url = "/api/tasks" <> maybe "" (\u -> "/" <> u) taskbody.uuid
      body = taskbody { status = maybe taskbody.status (\c -> Just $ if c then "completed" else "pending") completed }
  response <- H.liftAff $ AX.post AXResponse.json url $ AXBody.Json $ encodeJson body
  case response.body of
    Left err -> H.modify_ (_ { errors = [ Tuple "danger" $ AXResponse.printResponseFormatError err ] })
    Right json -> case decodeJson json of
      Left err -> H.modify_ (_ { errors = [ Tuple "danger" err] })
      Right (t :: Task) -> updateTasks

mkForm :: Maybe TaskBody -> H.ComponentHTML Query
mkForm taskFormBody =
  HH.div [ HP.classes $ map HH.ClassName [ "modal", "fade" ]
         , HP.id_ "task-form"
         , HP.tabIndex $ -1
         ]
  [ HH.div [ HP.class_ $ HH.ClassName "modal-dialog" ]
    [ HH.div [ HP.class_ $ HH.ClassName "modal-content" ]
      [ HH.div [ HP.class_ $ HH.ClassName "modal-header" ]
        [ HH.h5 [ HP.class_ $ HH.ClassName "modal-title" ]
          [ HH.text "Add new task" ]
        , HH.button [ HP.type_ HP.ButtonButton
                    , HP.class_ $ HH.ClassName "close"
                    , HE.onClick $ HE.input_ CloseForm
                    ]
          [ HH.span_ [ HH.text "x" ]]
        ]
      , HH.div [ HP.class_ $ HH.ClassName "modal-body" ]
        [ HH.div [ HP.class_ $ HH.ClassName "form-row" ]
          [ HH.div [ HP.classes $ map HH.ClassName ["form-group", "col"] ]
            [ HH.label [ HP.for "desc" ]
              [ HH.text "Description" ]
            , HH.input [ HP.type_ HP.InputText
                       , HP.id_ "desc"
                       , HP.class_ $ HH.ClassName "form-control"
                       , HP.placeholder "task"
                       , HP.value $ maybe "" _.desc taskFormBody
                       , HE.onValueChange $ HE.input UpdateDesc
                       ]
            ]
          ]
        , HH.div [ HP.class_ $ HH.ClassName "form-row" ]
          [ HH.div [ HP.classes $ map HH.ClassName ["form-group", "col"] ]
            [ HH.label [ HP.for "pro" ]
              [ HH.text "Project" ]
            , HH.input [ HP.type_ HP.InputText
                       , HP.id_ "pro"
                       , HP.class_ $ HH.ClassName "form-control"
                       , HP.placeholder "area.project"
                       , HP.value $ maybe "" _.project taskFormBody
                       , HE.onValueChange $ HE.input UpdateProj
                       ]
            ]
          ]
        , HH.div [ HP.class_ $ HH.ClassName "form-row" ]
          [ HH.div [ HP.classes $ map HH.ClassName ["form-group", "col"] ]
            [ HH.label [ HP.for "tags" ]
              [ HH.text "Tags" ]
            , HH.input [ HP.type_ HP.InputText
                       , HP.id_ "tags"
                       , HP.class_ $ HH.ClassName "form-control"
                       , HP.placeholder "tag1, tag2"
                       , HP.value $ maybe "" (Str.joinWith "," <<< _.tags) taskFormBody
                       , HE.onValueChange $ HE.input $ UpdateTags <<< prepareTags
                       ]
            ]
          ]
        , HH.div [ HP.class_ $ HH.ClassName "form-row" ]
          [ HH.div [ HP.classes $ map HH.ClassName ["form-group", "col"] ]
            [ HH.label [ HP.for "due" ]
              [ HH.text "Due date" ]
            , HH.input [ HP.type_ HP.InputDate
                       , HP.id_ "due"
                       , HP.class_ $ HH.ClassName "form-control"
                       , HP.placeholder "2019-08-01"
                       , HP.value $ maybe "" (maybe "" dateStr <<< _.due) taskFormBody
                       , HE.onValueChange $ HE.input $ UpdateDue <<< prepareDT
                       ]
            ]
          ]
        , HH.div [ HP.class_ $ HH.ClassName "form-row" ]
          [ HH.div [ HP.classes $ map HH.ClassName ["form-group", "col"] ]
            [ HH.label [ HP.for "wait" ]
              [ HH.text "Wait date" ]
            , HH.input [ HP.type_ HP.InputDate
                       , HP.id_ "wait"
                       , HP.class_ $ HH.ClassName "form-control"
                       , HP.placeholder "2019-06-30"
                       , HP.value $ maybe "" (maybe "" dateStr <<< _.wait) taskFormBody
                       , HE.onValueChange $ HE.input $ UpdateWait <<< prepareDT
                       ]
            ]
          ]
        ]
      , HH.div [ HP.class_ $ HH.ClassName "modal-footer" ]
        [ HH.button [ HP.type_ HP.ButtonButton
                    , HP.classes $ map HH.ClassName [ "btn", "btn-secondary" ]
                    , HE.onClick $ HE.input_ CloseForm
                    ]
          [ HH.text "Close" ]
        , HH.button [ HP.type_ HP.ButtonButton
                    , HP.classes $ map HH.ClassName [ "btn", "btn-primary" ]
                    , HE.onClick $ HE.input_ SaveTask ]
          [ HH.text "Save"]
        ]
      ]
    ]
  ]
  where
    prepareTags :: String -> Array String
    prepareTags = map Str.trim <<< Str.split (Str.Pattern ",")
    prepareDT :: String -> Maybe DateTime
    prepareDT s = DateTime <$> prepareDate s <*> midnight
    midnight :: Maybe Time
    midnight = Time <$> toEnum 0 <*> toEnum 0 <*> toEnum 0 <*> toEnum 0
    prepareDate :: String -> Maybe Date
    prepareDate "" = Nothing
    prepareDate s = case map Int.fromString $ Str.split (Str.Pattern "-") s of
      [Just y,Just m,Just d] -> canonicalDate <$> toEnum y <*> toEnum m <*> toEnum d
      _ -> Nothing
    dateStr :: ISO -> String
    dateStr = format (toUnfoldable [YearFull, Placeholder "-", MonthTwoDigits, Placeholder "-", DayOfMonthTwoDigits]) <<< unwrapISO

mkStatusLi :: String -> State -> H.ComponentHTML Query
mkStatusLi status st =
  HH.li [ HP.class_ $ HH.ClassName "nav-item" ]
  [ HH.a [ HP.classes $ map HH.ClassName classes
         , HE.onClick $ HE.input_ $ ChangeStatus status
         ] $
    [ HH.text status ]
    <>
    (if st.selectedStatus == status
     then [ HH.span [ HP.classes $ map HH.ClassName [ "badge", "badge-primary" ]]
            [ HH.text $ show $ length $ taskSelection st
            ]
          ]
     else [])
  ]
  where
    classes = if st.selectedStatus == status
              then ["active", "nav-link"]
              else ["nav-link"]

errToHtml :: Tuple String String -> H.ComponentHTML Query
errToHtml (Tuple c text) = HH.div [ HP.classes $ map HH.ClassName [ "alert", "alert-" <> c ]] [ HH.text text ]

projToHtml :: Maybe (Tuple String (Maybe String)) -> String -> Tuple String Int -> H.ComponentHTML Query
projToHtml selectedProject area (Tuple proj cnt) =
  HH.li [ HP.classes $ map HH.ClassName classes ]
  [ HH.div [ HP.classes $ map HH.ClassName [ "d-flex", "w-100", "justify-content-between", "align-items-center"]
           , HE.onClick $ HE.input_ $ ChangeProject $ Tuple area $ if proj == "NA" then Just "" else Just proj ]
    [ HH.text proj
    , HH.span [ HP.classes $ map HH.ClassName [ "badge", "badge-primary", "badge-pill" ]]
      [ HH.text $ show cnt
      ]
    ]
  ]
  where classes = if selectedProject == Just (Tuple area $ Just proj) || selectedProject == Just (Tuple area Nothing)
                  then ["list-group-item", "active"]
                  else ["list-group-item"]

areaToHtml :: Maybe (Tuple String (Maybe String)) -> Area -> Array (H.ComponentHTML Query)
areaToHtml selectedProject (Tuple area projects) =
  [ HH.h3 [ HP.classes $ map HH.ClassName ["mt-2", "pt-2"]]
    [ HH.a [ HP.class_ $ HH.ClassName "text-decoration-none"
           , HP.href "#"
           , HE.onClick $ HE.input_ $ ChangeProject $ Tuple area Nothing
           ]
      [ HH.img [ HP.src "https://img.icons8.com/material-outlined/24/000000/group-of-projects.png" ]
      , HH.text $ " " <> area
      ]
    ]
  , HH.ul [ HP.class_ $ HH.ClassName "list-group" ] $
    map (projToHtml selectedProject area) projects
  ]

tagToHtml :: Array String -> Tag -> H.ComponentHTML Query
tagToHtml selected (Tuple tag cnt) =
  HH.div [ HP.classes $ map HH.ClassName [ "btn-group", "btn-group-toggle", "mb-2", "pb-2" ]]
  [ HH.button [ HP.classes $ map HH.ClassName classes
              , HE.onClick $ HE.input_ $ ToggleTag tag
              ]
    [ HH.text tag
    , HH.span [ HP.classes $ map HH.ClassName [ "badge", "badge-primary" ]]
      [ HH.text $ show cnt ]
    ]
  ]
  where classes = if tag `elem` selected
                  then [ "btn", "btn-primary", "active" ]
                  else [ "btn", "btn-primary" ]

taskToHtml :: State -> Task -> H.ComponentHTML Query
taskToHtml st task@(Task t) =
  HH.div [ HP.classes $ map HH.ClassName ["form-check", "my-1", "py-1"]]
  [ HH.input [ HP.classes $ map HH.ClassName [ "form-check-input" ]
             , HP.type_ HP.InputCheckbox
             , HP.checked $ t.status == "completed"
             , HP.id_ $ "c" <> t.uuid
             , HE.onChecked $ HE.input $ SetCompletion $ taskToBody task
             ]
  , HH.label [ HP.classes $ map HH.ClassName [ "form-check-label" ]
             -- , HP.for $ "c" <> t.uuid
             ] $
    [ HH.span [ HP.class_ $ HH.ClassName "mr-3"
              -- , HE.onClick $ HE.input_ $ Toggle t.uuid
              ]
      [ HH.text t.desc ]
    , HH.span [ HP.classes $ map HH.ClassName [ "badge", if st.selectedProject == Just (strToProjName t.proj) then "badge-primary" else "badge-secondary", "mx-2" ]
              , HE.onClick $ HE.input_ $ ChangeProject $ strToProjName t.proj
              ]
      [ HH.img [ HP.src "https://img.icons8.com/material-outlined/24/FFFFFF/task.png"]
      , HH.text $ t.proj
      ]
    ]
    <>
    mkDue t.due
    <>
    map mkTag t.tags
    <>
    mkButtons
  ]
  where
    mkTag tag =
      HH.span [ HP.classes $ map HH.ClassName [ "badge"
                                              , if tag`elem`st.selectedTags
                                                then "badge-success"
                                                else "badge-warning"
                                              , "mx-1"
                                              ]
              , HE.onClick $ HE.input_ $ ToggleTag tag
              ]
      [ HH.img [ HP.src "https://img.icons8.com/material-outlined/24/000000/tags.png" ]
      , HH.text tag
      ]
    mkDue Nothing = []
    mkDue (Just due) =
      [ HH.span [ HP.classes $ map HH.ClassName [ "badge", "badge-danger", "mx-2" ] ]
        [ HH.img [ HP.src "https://img.icons8.com/material-outlined/24/FFFFFF/overtime.png" ]
        , HH.text $ dateToStr due
        ]
      ]
    mkButtons =
      [ HH.span [ HP.classes $ map HH.ClassName [ "badge", "mx-3" ]
                , HE.onClick $ HE.input_ $ ShowForm $ taskToBody task ]
        [ HH.img [ HP.src "https://img.icons8.com/material-outlined/24/000000/edit.png" ]
        ]
      ]
    dateToStr dt = let
      ymd = date dt
      hms = time dt
      h = hour hms
      m = minute hms
      formatter = [YearFull, Placeholder "-", MonthTwoDigits, Placeholder "-", DayOfMonthTwoDigits]
                  <>
                  (if Just h==toEnum 21 && Just m==toEnum 0
                   then []
                   else [Placeholder " ", Hours24, Placeholder ":", MinutesTwoDigits])
      in format (toUnfoldable formatter) dt

strToProjName :: String -> ProjName
strToProjName = CU.toCharArray >>> span ((/=) '.') >>> toProjName
  where toProjName l = Tuple (CU.fromCharArray l.init) $ case NArr.fromArray l.rest of
          Nothing -> Nothing
          Just r -> Just $ CU.fromCharArray $ NArr.tail r

constructAreas :: Array Task -> Array Area
constructAreas = map (strToProjName <<< (\(Task t) -> t.proj)) >>>
                 sort >>>
                 groupBy ((==)`on`fst) >>>
                 map toArea
  where toArea :: NArr.NonEmptyArray ProjName -> Area
        toArea l = Tuple (fst $ NArr.head l) $ mkProj $ NArr.mapMaybe snd l
        mkProj :: Array String -> Array Proj
        mkProj = sort >>> group >>> map toProj
        toProj :: NArr.NonEmptyArray String -> Proj
        toProj l = Tuple (NArr.head l) (NArr.length l)

constructTags :: Array Task -> Array Tag
constructTags = concatMap (\(Task t) -> t.tags) >>>
                sort >>>
                group >>>
                map (\l -> Tuple (NArr.head l) (NArr.length l))

sameProj :: Maybe (Tuple String (Maybe String)) -> String -> Boolean
sameProj Nothing _ = true
sameProj (Just (Tuple area Nothing)) s = case CU.stripPrefix (Str.Pattern area) s of
  Nothing -> false
  Just _ -> true
sameProj (Just (Tuple area (Just proj))) s = area <> "." <> proj == s

hasTags :: Array String -> Array String -> Boolean
hasTags selected tags = all (\s -> s`elem`tags) selected

taskSelection :: State -> Array Task
taskSelection st = sortBy (compare`on`(\(Task t) -> -t.urg)) $
                   filter (\(Task t) -> sameProj st.selectedProject t.proj && hasTags st.selectedTags t.tags)
                   st.tasks

ui :: H.Component HH.HTML Query State Void Aff
ui =
  H.component
  { initialState: identity
  , render
  , eval
  , receiver: const Nothing
  }
  where
    render :: State -> H.ComponentHTML Query
    render st =
      HH.div [ HP.class_ $ HH.ClassName "container-fluid" ]
      [ HH.h1 [ HP.classes $ map HH.ClassName [ "display-1", "text-center" ]] [ HH.text "Tasks" ]
      , HH.div [ HP.class_ $ HH.ClassName "row" ]
        [ HH.ul_ $ map errToHtml st.errors
        ]
      , HH.div [ HP.class_ $ HH.ClassName "row" ]
        [ HH.div [ HP.classes $ map HH.ClassName [ "col", "col-md-4", "col-lg-3", "col-xl-2" ]] $
          HH.h2_ [ HH.text "Projects"]
          :
          concatMap (areaToHtml st.selectedProject) st.projects
        , HH.div [ HP.classes $ map HH.ClassName [ "col", "col-md-7", "col-lg-8", "col-xl-9" ]] $
          HH.ul [ HP.classes $ map HH.ClassName [ "nav", "nav-tabs" ]]
          [ mkStatusLi "pending"   st
          , mkStatusLi "waiting"   st
          , mkStatusLi "completed" st
          , HH.li [ HP.class_ $ HH.ClassName "nav-item" ]
            [ HH.button [ HP.classes $ map HH.ClassName [ "btn", "btn-secondary" ]
                        , HE.onClick $ HE.input_ $ ShowForm emptyTaskBody
                        ]
              [ HH.text "Add" ]
            ]
          , HH.li [ HP.class_ $ HH.ClassName "nav-item" ]
            [ HH.button [ HP.classes $ map HH.ClassName [ "btn btn-primary" ]
                        , HE.onClick $ HE.input_ Sync
                        ]
              [ HH.text "Sync"
              ]
            ]
          ]
          :
          mkForm st.taskFormBody
          :
          map (taskToHtml st) (taskSelection st)
        , HH.div [ HP.classes $ map HH.ClassName [ "col", "col-md-1", "col-lg-1", "col-xl-1" ] ] $
          [ HH.h2_ [ HH.text "Tags"]
          , HH.div [ HP.classes $ map HH.ClassName [ "btn-group", "btn-group-vertical" ]] $
            map (tagToHtml st.selectedTags) st.tags
          ]
        ]
      ]

    eval :: Query ~> H.ComponentDSL State Query Void Aff
    eval = case _ of
      Sync next -> do
        updateTasks
        pure next
      ChangeStatus status next -> do
        H.modify_ (_ { selectedStatus = status, tasks = [] })
        updateTasks
        pure next
      ChangeProject (Tuple area mproj) next -> do
        mcur <- H.gets _.selectedProject
        if mcur == Just (Tuple area mproj)
          then H.modify_ (_ { selectedProject = Nothing})
          else H.modify_ (_ { selectedProject = Just (Tuple area mproj)})
        updateTasks
        pure next
      ToggleTag tag next -> do
        H.modify_ (\st -> st { selectedTags = if tag `elem` st.selectedTags
                                              then delete tag st.selectedTags
                                              else insert tag st.selectedTags })
        pure next
      ShowForm taskbody next -> do
        H.modify_ (_ { taskFormBody = Just taskbody })
        H.liftAff $ showTaskForm true
        pure next
      UpdateDesc v next -> do
        H.modify_ (\s -> s { taskFormBody = map (_ {desc=v}) s.taskFormBody})
        pure next
      UpdateProj v next -> do
        H.modify_ (\s -> s { taskFormBody = map (_ {project=v}) s.taskFormBody})
        pure next
      UpdateTags v next -> do
        H.modify_ (\s -> s { taskFormBody = map (_ {tags=v}) s.taskFormBody})
        pure next
      UpdateDue v next -> do
        H.modify_ (\s -> s { taskFormBody = map (_ {due=map ISO v}) s.taskFormBody})
        pure next
      UpdateWait v next -> do
        H.modify_ (\s -> s { taskFormBody = map (_ {wait=map ISO v}) s.taskFormBody})
        pure next
      CloseForm next -> do
        H.modify_ (_ {taskFormBody = Nothing})
        H.liftAff $ showTaskForm false
        pure next
      SaveTask next -> do
        mtaskBody <- H.gets _.taskFormBody
        case mtaskBody of
          Nothing -> pure next
          Just taskBody -> do
            -- H.modify_ (\st -> st { errors = [Tuple "info" $ "Save task: " <> show st.taskFormBody], taskFormBody = Nothing })
            saveTask taskBody Nothing
            H.liftAff $ showTaskForm false
            pure next
      SetCompletion body completed next -> do
        -- H.modify_ (_ { errors = [Tuple "info" $ "Set " <> show body.uuid <> " as " <> show completed] })
        saveTask body $ Just completed
        pure next


